# Todo App

A todo ReactJS web application.

https://todo.air.in.rs/

Author: Aleksa Otasevic

Email: aleksa.otasevic@yahoo.com

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Media files source:

**Background image:**

Author: Masahiro Miyagi

Source: https://unsplash.com/photos/JO9H8b50P0M
